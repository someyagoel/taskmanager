### Task Management System

Problem Statement: Design and implement a task management system using Java and SpringBoot

### Assumptions:-

- TaskProcessor processes the task for the current date according to priority. The priority is a random number which is auto-generated.

- A user can only update task's due date based on entered TaskId

### External dependencies

1.'hibernate-core' - For Hibernate(ORM) framework

2.'org.junit.jupiter & org.junit.platform & mockito-core'- Unit testing framework

3.'com.h2database' - For In Built H2 Database connection


### APIs

1. `/taks`
This API fetches all the tasks from the backend so that user can view processed as well as new task. The API return the data sorted on dueDate and priority.This helps user in managing the tasks.This API is called by frontend asynchronously to load the page with tasks without refreshing. 


*Response* :-  200 if the request is successful
```
{
   {
        "id": 1,
        "createdAt": "2020-01-21T13:53:13.000+0000",
        "dueDate": "2020-01-24T21:58:51.000+0000",
        "resolvedAt": null,
        "updatedAt": "2020-01-21T13:53:13.000+0000",
        "title": "Task",
        "description": "New Task created at: Tue Jan 21 14:53:12 CET 2020",
        "priority": 9,
        "status": "Created"
    },
    {
        "id": 4,
        "createdAt": "2020-01-21T13:57:10.000+0000",
        "dueDate": "2020-01-25T06:19:06.000+0000",
        "resolvedAt": null,
        "updatedAt": "2020-01-21T13:57:10.000+0000",
        "title": "Task",
        "description": "New Task created at: Tue Jan 21 14:57:09 CET 2020",
        "priority": 1,
        "status": "Created"
    }
}
```

2./task
This API allow user to update the due date of the task. 

*Request* :
 ```
{
        "id": 1,
        "dueDate": "2020-01-24T21:58:51.000+0000"
}
```
*Response*
```
{
    "id": 58,
    "createdAt": "2020-01-24T12:42:04.779+0000",
    "dueDate": "2020-02-26T23:00:00.000+0000",
    "resolvedAt": null,
    "updatedAt": "2020-01-24T12:53:25.278+0000",
    "title": "Task",
    "description": "New Task created at: Fri Jan 24 13:42:04 CET 2020",
    "priority": 11,
    "status": "Created"
}
```

#### Building and running the project

 1) `mvn clean` and `mvn package -Dmaven.test.skip=true`
 
 2) To run the application in docker :-
 
    `docker build -t boot-taskmanager .`
 
    `docker run -p 8080:8080 -t boot-taskmanager`

 3) In Postman or any other REST client hit the APIs mentioned above :- `http://localhost:8080/{request}`
 
 4) For UI, go to `localhost:8080/` , This will bring to the UI page
 
 5) Unit test and integration test are added in src/test/java
 
 6) Frontend changes present in index.html
 

### Additional Comments

- Added interfaces for each class as it will serve as a contract for having common behaviour in the impl classes. This design will help in extending the application easily.

- Task of the same day would be processed on the basis of priority. For now The priority are random numbers generated.

- The scheduler creates the Task at random interval with DueDate within a week

- The processor continuouslly pools the database and add the task in queues for further processing on basis of priority

- The Tasks are sorted according to due date and priority

- Addined ajax call to load all tasks aynchronously without refreshing the page in index.html

- Added an initial delay of 30s to add the first task via scheduler(reason : to let the application load properly)

### Screenshot

![Screenshot](taskmanagementsystem.png)


