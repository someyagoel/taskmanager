package com.task.dto;

import java.util.Date;

public class TaskDto {

	private Long id;
	private Date createdAt;
	private Date dueDate;
	private Date resolvedAt;
	private Date updatedAt;
	private String title;
	private String description;
	private int priority;
	private String status;

	public Long getId() {
		return id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public Date getResolvedAt() {
		return resolvedAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public int getPriority() {
		return priority;
	}

	public String getStatus() {
		return status;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public void setResolvedAt(Date resolvedAt) {
		this.resolvedAt = resolvedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
