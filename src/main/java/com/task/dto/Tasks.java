package com.task.dto;

import java.util.ArrayList;
import java.util.List;

public class Tasks {

	private List<TaskDto> taskList;

	public List<TaskDto> getTaskList() {
		if (taskList == null) {
			taskList = new ArrayList<>();
		}
		return taskList;
	}

	public void setTaskList(List<TaskDto> employeeList) {
		this.taskList = employeeList;
	}

}
