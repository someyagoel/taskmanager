package com.task.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task.dto.TaskDto;
import com.task.dto.Tasks;
import com.task.exception.InvalidDateException;
import com.task.exception.InvalidTaskException;
import com.task.mapper.TaskMapper;
import com.task.model.Task;
import com.task.service.TaskService;
import com.task.util.TasksUtil;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/")
public class TaskResource {

	@Autowired
	TaskService taskService;
	@Autowired
	TasksUtil taskUtil;
	@Autowired
	TaskMapper taskMapper;

	@GetMapping(value ="/tasks")
	public Tasks getTasks() {
		Tasks taskList = new Tasks();
		List<Task> tasks=taskService.getTasks();
		taskList.setTaskList(taskMapper.convertTasktoTaskDTo(tasks));
		return taskList;
	}

	@PostMapping(value="/task")
	public TaskDto updateTask(@RequestBody TaskDto t) throws InvalidTaskException, InvalidDateException {
		taskUtil.validate(t);
		Task task = taskMapper.convertDtotoTask(t);
		task=taskService.updateTask(task);
		return taskMapper.convertTaskToTaskDto(task);
	}

}
