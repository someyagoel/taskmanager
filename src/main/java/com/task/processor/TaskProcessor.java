package com.task.processor;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.PriorityBlockingQueue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.task.configuration.TaskConstants;
import com.task.configuration.TaskProcessingQueue;
import com.task.dao.TaskDAO;
import com.task.model.Task;

/**
 *  This class processes the new task in the queue for the current day
 */

@Component
public final class TaskProcessor {

	@Autowired
	TaskDAO taskDAO;
	PriorityBlockingQueue<Task> taskQueue = TaskProcessingQueue.getTaskprocessqueue();
	
	// This method polls for the new task every 5 minutes in the database and processes it by the priority
	@Scheduled(fixedRate = 5000, initialDelay = 40000)
	public void processTask() {
		Collection<Task> todayTasks = taskDAO.fetchCurrentTasks(TaskConstants.TASK_CREATED);
		taskQueue.addAll(todayTasks);
		while (!taskQueue.isEmpty()) {
			Task t = taskQueue.poll();
			{
				taskDAO.update(t.getId(), TaskConstants.TASK_PROCESSED);
			}
		}

	}

}