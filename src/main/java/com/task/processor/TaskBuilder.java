package com.task.processor;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.task.configuration.TaskConstants;
import com.task.dao.TaskDAO;
import com.task.model.Task;
import com.task.util.TasksUtil;


/**
 * This class generates new Tasks at random intervals and saves it to database
 *
 */

@Component
public class TaskBuilder {

	@Autowired
	TasksUtil taskUtil;
	@Autowired
	TaskDAO taskDAO;

	@Scheduled(fixedRate = 20000, initialDelay = 30000)
	public void execute() {
		Task t = new Task();
		t.setCreatedAt(new Date());
		t.setUpdatedAt(new Date());
		t.setDescription("New Task created at: " + t.getCreatedAt());
		t.setStatus(TaskConstants.TASK_CREATED);
		t.setTitle(TaskConstants.TASK_TITLE);
		t.setPriority(taskUtil.generateRandomNumber());
		t.setDueDate(taskUtil.between(new Date(), taskUtil.endDate(new Date())));
		taskDAO.save(t);
	}

}
