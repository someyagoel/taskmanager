package com.task.service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.task.dao.TaskDAO;
import com.task.model.Task;
import com.task.util.DateComparator;
import com.task.util.PriorityComparator;
import com.task.util.TasksUtil;

@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	TaskDAO taskDAO;
	@Autowired
	TasksUtil taskUtil;

	@Override
	public List<Task> getTasks() {
		List<Task> tasks = taskDAO.findAll();
		Collections.sort(tasks, new DateComparator().thenComparing(new PriorityComparator()));
		return tasks;
	}

	@Override
	public Task updateTask(Task task) {
		taskDAO.updateDueDate(task.getId(), task.getDueDate());
		Optional<Task> updatedTask = taskDAO.findById(task.getId());
		return updatedTask.get();
	}

}
