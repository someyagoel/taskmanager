package com.task.service;

import java.util.List;

import com.task.model.Task;

public interface TaskService {

	List<Task> getTasks();

	Task updateTask(Task task);

}