package com.task.mapper;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.task.dto.TaskDto;
import com.task.model.Task;

@Component
public class TaskMapper {
	
	public List<TaskDto> convertTasktoTaskDTo(List<Task> taskList) {
		List<TaskDto> listDTO = new LinkedList<TaskDto>();
		for (Task task : taskList) {
			TaskDto t = new TaskDto();
			t.setCreatedAt(task.getCreatedAt());
			t.setDescription(task.getDescription());
			t.setDueDate(task.getDueDate());
			t.setId(task.getId());
			t.setPriority(task.getPriority());
			t.setResolvedAt(task.getResolvedAt());
			t.setStatus(task.getStatus());
			t.setTitle(task.getTitle());
			t.setUpdatedAt(task.getUpdatedAt());
			listDTO.add(t);
		}
		return listDTO;
	}
	
	public TaskDto convertTaskToTaskDto(Task task) {
		TaskDto t = new TaskDto();
		t.setCreatedAt(task.getCreatedAt());
		t.setDescription(task.getDescription());
		t.setDueDate(task.getDueDate());
		t.setId(task.getId());
		t.setPriority(task.getPriority());
		t.setResolvedAt(task.getResolvedAt());
		t.setStatus(task.getStatus());
		t.setTitle(task.getTitle());
		t.setUpdatedAt(task.getUpdatedAt());
		return t;
	}

	public Task convertDtotoTask(TaskDto task) {
		Task t = new Task();
		t.setCreatedAt(task.getCreatedAt());
		t.setDescription(task.getDescription());
		t.setDueDate(task.getDueDate());
		t.setId(task.getId());
		t.setPriority(task.getPriority());
		t.setResolvedAt(task.getResolvedAt());
		t.setStatus(task.getStatus());
		t.setTitle(task.getTitle());
		t.setUpdatedAt(task.getUpdatedAt());
		return t;
	}

}
