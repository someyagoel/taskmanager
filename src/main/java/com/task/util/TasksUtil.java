package com.task.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.task.configuration.TaskConstants;
import com.task.dao.TaskDAO;
import com.task.dto.TaskDto;
import com.task.exception.InvalidDateException;
import com.task.exception.InvalidTaskException;
import com.task.model.Task;

// Helper class 
@Component
public class TasksUtil {

	@Autowired
	TaskDAO taskDAO;

	// generates random number
	public int generateRandomNumber() {
		int priority = (int) ((Math.random() * (TaskConstants.MAX_PRIORITY - TaskConstants.MIN_PRIORITY))
				+ TaskConstants.MIN_PRIORITY);
		return priority;
	}

	// generates random date
	public Date between(Date startInclusive, Date endExclusive) {
		long startMillis = startInclusive.getTime();
		long endMillis = endExclusive.getTime();
		long randomMillisSinceEpoch = ThreadLocalRandom.current().nextLong(startMillis, endMillis);
		return new Date(randomMillisSinceEpoch);
	}

	public Date StartDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 1);
		Date modifiedDate = cal.getTime();
		return modifiedDate;
	}

	public Date endDate(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 7);
		Date modifiedDate = cal.getTime();
		return modifiedDate;
	}

	public TaskDto validate(TaskDto taskDto) throws InvalidTaskException, InvalidDateException {

		Optional<Task> t = taskDAO.findById(taskDto.getId());
		Date today = new Date();
		if (t.isEmpty()) {
			throw new InvalidTaskException("Enter the correct Id.Task doest not exist");
		} else if (t.get().getDueDate().equals(today) || t.get().getDueDate().compareTo(today) < 0) {
			throw new InvalidDateException("Cannot schedule todays or past task.Please select a future task");
		} else if (taskDto.getDueDate().compareTo(today) < 0) {
			throw new InvalidDateException("Select a valid due Date");
		}
		return taskDto;

	}

	

}
