package com.task.util;

import java.util.Comparator;

import com.task.model.Task;


// Sorts the task on the basis of Due Date
public class DateComparator implements Comparator<Task>{

	@Override
	public int compare(Task t1, Task t2) {
		
		return t1.getDueDate().compareTo(t2.getDueDate());
	}

}
