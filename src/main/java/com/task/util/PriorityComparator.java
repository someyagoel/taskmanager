package com.task.util;

import java.util.Comparator;

import com.task.model.Task;

//Sorts the task on the basis of Priority
public class PriorityComparator implements Comparator<Task> {
	
	public int compare(Task t1, Task t2) {
		return t1.getPriority()-t2.getPriority();
	}

}
