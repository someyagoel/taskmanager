package com.task.configuration;

import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;

import com.task.model.Task;


/**
 *  The class creates the processing queue for task to be executed in priority
 *
 */
public class TaskProcessingQueue {
	
	private static Comparator<Task> prioritySorter = Comparator.comparing(Task::getPriority);
	
	private final static PriorityBlockingQueue<Task> taskProcessQueue = new PriorityBlockingQueue<Task>(
			TaskConstants.QUEUE_CAPACITY, prioritySorter);
	
	public static PriorityBlockingQueue<Task> getTaskprocessqueue() {
		return taskProcessQueue;
	}

}
