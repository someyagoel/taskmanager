package com.task.configuration;

public class TaskConstants {
	
	public static final String TASK_CREATED = "Created";
	public static final String TASK_PROCESSED = "Processed";
	public static final String TASK_TITLE = "Task";
	public static final int MAX_PRIORITY = 10;
	public static final int MIN_PRIORITY = 1;
	public static final int QUEUE_CAPACITY = 100;

}
