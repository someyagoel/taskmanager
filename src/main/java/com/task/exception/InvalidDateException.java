package com.task.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class InvalidDateException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public InvalidDateException(String message)
	{
		super(message);
	}

}
