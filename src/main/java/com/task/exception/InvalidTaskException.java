package com.task.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Task in invalid")
public class InvalidTaskException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public InvalidTaskException(String message)
	{
		super(message);
	}

}
