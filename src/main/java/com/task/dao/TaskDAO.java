package com.task.dao;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.task.model.Task;

@Repository
public interface TaskDAO extends JpaRepository<Task, Long> {

	@Query("from Task t where t.dueDate = current_date() and t.status = :status")
	List<Task> fetchCurrentTasks(String status);
	
	@Transactional
	@Modifying
	@Query("UPDATE Task t SET t.dueDate = :dueDate, t.updatedAt=current_date()  WHERE t.id = :taskId")
	void updateDueDate(@Param("taskId") Long taskId, @Param("dueDate") Date dueDate);
	
	
	@Transactional
	@Modifying
	@Query("UPDATE Task t SET t.status = :status, t.updatedAt=current_date(),t.resolvedAt=current_date() WHERE t.id = :taskId")
	void update(@Param("taskId") Long taskId, @Param("status") String status);
	

}