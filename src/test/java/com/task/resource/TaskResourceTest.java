package com.task.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.task.dto.TaskDto;
import com.task.dto.Tasks;
import com.task.exception.InvalidDateException;
import com.task.exception.InvalidTaskException;
import com.task.mapper.TaskMapper;
import com.task.model.Task;
import com.task.service.TaskService;
import com.task.util.TasksUtil;

@RunWith(MockitoJUnitRunner.class)
public class TaskResourceTest {

	@Mock
	TaskService taskService;
	@Mock
	TaskMapper taskMapper;
	@Mock
	TasksUtil taskUtil;
	@InjectMocks
	TaskResource taskResource;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getTasks() {
		Mockito.when(taskService.getTasks()).thenReturn(getTaskList());
		Mockito.when(taskMapper.convertTasktoTaskDTo(getTaskList())).thenReturn(getTaskDtoList());
		Tasks tasklist = taskResource.getTasks();
		assertEquals(tasklist.getTaskList().size(), 0);
	}

	@Test
	public void updateTasks_success() throws InvalidTaskException, InvalidDateException {
		Task task = new Task();
		task.setId(1l);
		TaskDto t = new TaskDto();
		t.setId(1l);
		Mockito.when(taskService.updateTask(task)).thenReturn(task);
		Mockito.when(taskUtil.validate(t)).thenReturn(t);
		Mockito.when(taskMapper.convertDtotoTask(t)).thenReturn(task);
		Mockito.when(taskMapper.convertTaskToTaskDto(task)).thenReturn(t);
		assertEquals(taskResource.updateTask(t).getId(), 1l);
	}

	@Test
	public void updateTasks_badRequest() throws InvalidTaskException, InvalidDateException {
		Task task = new Task();
		task.setId(1l);
		TaskDto t = new TaskDto();
		t.setId(1l);
		Mockito.when(taskMapper.convertDtotoTask(t)).thenReturn(task);
		Mockito.when(taskService.updateTask(task)).thenReturn(task);
		doThrow(new InvalidTaskException("Task is invalid")).when(taskUtil).validate(t);
		Exception exception = assertThrows(InvalidTaskException.class, () -> {
			taskResource.updateTask(t);
		});
		assertEquals(exception.getMessage(), "Task is invalid");
	}

	@Test
	public void updateTasks_badRequest_InvalidDate() throws InvalidTaskException, InvalidDateException {
		Task t = new Task();
		t.setId(1l);
		TaskDto task = new TaskDto();
		task.setId(1l);
		Mockito.when(taskMapper.convertDtotoTask(task)).thenReturn(t);
		Mockito.when(taskService.updateTask(t)).thenReturn(t);
		doThrow(new InvalidDateException("Date is invalid")).when(taskUtil).validate(task);
		Exception exception = assertThrows(InvalidDateException.class, () -> {
			taskResource.updateTask(task);
		});
		assertEquals(exception.getMessage(), "Date is invalid");
	}

	private List<Task> getTaskList() {
		Task t1 = new Task();
		t1.setId(1l);
		t1.setTitle("Task 1");
		t1.setDueDate(new Date());
		t1.setPriority(1);

		Task t2 = new Task();
		t2.setId(1l);
		t2.setTitle("Task 2");
		t2.setDueDate(new Date());
		t1.setPriority(2);

		List<Task> taskList = new LinkedList<Task>();
		taskList.add(t1);
		taskList.add(t2);
		return taskList;
	}
	
	private List<TaskDto> getTaskDtoList() {
		TaskDto t1 = new TaskDto();
		t1.setId(1l);
		t1.setTitle("Task 1");
		t1.setDueDate(new Date());
		t1.setPriority(1);

		TaskDto t2 = new TaskDto();
		t2.setId(1l);
		t2.setTitle("Task 2");
		t2.setDueDate(new Date());
		t1.setPriority(2);

		List<TaskDto> taskList = new LinkedList<TaskDto>();
		taskList.add(t1);
		taskList.add(t2);
		return taskList;
	}

}
