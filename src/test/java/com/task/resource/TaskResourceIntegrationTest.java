package com.task.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;

import com.task.TaskManagementSystemApplication;
import com.task.dto.TaskDto;
import com.task.dto.Tasks;
import com.task.exception.InvalidDateException;
import com.task.exception.InvalidTaskException;

@SpringBootTest(classes = TaskManagementSystemApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class TaskResourceIntegrationTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Sql({"schema.sql" , "data.sql"  })
	@Test
	public void testgetAllTasks_success() {
		assertTrue(this.restTemplate.getForObject("http://localhost:" + port + "/tasks", Tasks.class).getTaskList()
				.size() > 0);
	}

	@Sql({"schema.sql" , "data_update.sql"  })
	@Test
	public void testUpdateTask() {
		TaskDto task = new TaskDto();
		task.setId(8000l);
		task.setCreatedAt(new Date());
		task.setDueDate(new Date());
		TaskDto t = this.restTemplate.postForObject("http://localhost:" + port + "/task", task, TaskDto.class);
		assertEquals(1l, t.getId());
	}

	@Sql({"schema.sql" , "data_update.sql"  })
	@Test
	public void testUpdateTask_Exception() {
		TaskDto task = new TaskDto();
		task.setId(1678l);
		task.setCreatedAt(new Date());
		task.setDueDate(new Date());
		assertThrows(InvalidTaskException.class, () -> {
			this.restTemplate.postForObject("http://localhost:" + port + "/task", task, TaskDto.class);
		});
	}
	
	@Sql({"schema.sql" , "data_update.sql"  })
	@Test
	public void testUpdateTask_InValidDateException() {
		TaskDto task = new TaskDto();
		task.setId(1l);
		task.setCreatedAt(new Date());
		task.setDueDate(new Date());
		assertThrows(InvalidDateException.class, () -> {
			this.restTemplate.postForObject("http://localhost:" + port + "/task", task, TaskDto.class);
		});
	}

}
