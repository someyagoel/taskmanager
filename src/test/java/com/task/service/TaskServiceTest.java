package com.task.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.task.dao.TaskDAO;
import com.task.exception.InvalidDateException;
import com.task.exception.InvalidTaskException;
import com.task.model.Task;

@RunWith(MockitoJUnitRunner.class)
public class TaskServiceTest {

	@Mock
	TaskDAO taskDAO;
	@InjectMocks
	TaskServiceImpl taskService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getTasks_success_tasks() {
		Mockito.when(taskDAO.findAll()).thenReturn(getTaskList());
		List<Task> task = taskService.getTasks();
		assertEquals(task.size(), 2);
	}

	@Test
	public void updateTask_success() throws InvalidTaskException, InvalidDateException {
		Optional<Task> task = Optional.of(new Task());
		task.get().setId(1l);
		task.get().setTitle("Task 1");	
		task.get().setDueDate(new Date());
		Task t =null;
		Mockito.when(taskDAO.findById(task.get().getId())).thenReturn(task);
		Mockito.doNothing().when(taskDAO).updateDueDate(task.get().getId(), task.get().getDueDate());
		t = taskService.updateTask(task.get());
		assertTrue(t.getId().equals(task.get().getId()));
	}

	private List<Task> getTaskList() {
		Task t1 = new Task();
		t1.setId(1l);
		t1.setTitle("Task 1");
		t1.setDueDate(new Date());
		t1.setPriority(1);

		Task t2 = new Task();
		t2.setId(1l);
		t2.setTitle("Task 2");
		t2.setDueDate(new Date());
		t2.setPriority(2);

		List<Task> taskList = new LinkedList<Task>();
		taskList.add(t1);
		taskList.add(t2);
		return taskList;
	}

}
